#include<stdio.h>
#include <string.h>
#include<limits.h>
int w[130],max=0;
char s[100000];
struct
{
int rand;
int verif;
int cont;
char inf;
char cod[30];
int stanga,dreapta;
}v[100000],aux;
struct
{
    char inf[30];
}COD[130];
int maxim(int a,int b)
{
    if(a>b)
        return a;
    return b;
}
int min1(int n)
{
    int a,min=INT_MAX,b,r;
    for(a=0;a<n;a++)
        {if(v[a].cont<min && v[a].verif!=-1)
            min=v[a].cont, b=a,r=v[a].rand;
         else if(v[a].cont==min && v[a].verif!=-1 && v[a].rand>r)
            min=v[a].cont, b=a,r=v[a].rand;}
    return b;
}
int min2(int k,int n)
{
    int a,min=INT_MAX,b,r;
    for(a=0;a<n;a++)
        {if(v[a].cont<min && v[a].verif!=-1 && a!=k)
            min=v[a].cont, b=a;
         else if(v[a].cont==min && v[a].verif!=-1 && a!=k && v[a].rand>r)
            min=v[a].cont, b=a,r=v[a].rand;}
    return b;
}
void codificare()
{
	FILE *f;
	f=fopen("test.txt", "r");
    char c;
	int i=0,j,a,n;
    while(1)
   {
       if(feof(f))
      {
         break ;
      }
      s[i++]=fgetc(f);
      printf("%c", s[i-1]);
   }
   puts("");
   for(j=0;j<i;j++)
   {
       a=s[j];
       w[a]++;
       max++;
   }max--;
    //for(j=0;j<128;j++)
      //  printf("%d ", w[j]);
    i=0;
    for(j=0;j<128;j++)
        if(w[j]!=0)
    {
        c=(char)(j);
        v[i].inf=c;
        v[i].cont=w[j];
        i++;
    }
    n=i;
    for(i=0;i<n-1;i++)
        for(j=i+1;j<n;j++)
            if(v[j].cont<v[i].cont)
            {
               aux=v[i];
               v[i]=v[j];
               v[j]=aux;
            }
   // for(j=0;j<n;j++)
     //   printf("%d %c\n", v[j].cont,v[j].inf);
    //printf("%d\n", max);
    for(j=0;j<n;j++)
        v[j].verif=1,v[j].rand=1;
    i=0;j=n-1;
    int k1,k2;
    while(i!=max)
    {
        i=0;j++;
        v[j].verif=1;
        k1=min1(j);k2=min2(k1,j);
        i=v[k1].cont+v[k2].cont;
        v[j].cont=i;
        v[j].stanga=k2;
        v[j].dreapta=k1;
        v[j].rand=maxim(v[k1].rand,v[k2].rand)+1;
        v[k1].verif=-1;
        v[k2].verif=-1;
    }
    int m=j+1;
    //for(j=n;j<m;j++)
      //  printf("v[%d]s=%d v[%d]d=%d    v=%d\n",j,v[j].stanga,j,v[j].dreapta,v[j].cont);
    char st='0',dr='1';
    int z,x;
    for(j=m-1;j>=n;j--)
        {
            z=v[j].stanga;
            x=v[j].dreapta;
            strcpy(v[z].cod,v[j].cod);
            v[z].cod[strlen(v[z].cod)]=st;
            v[z].cod[strlen(v[z].cod)+1]='\0';

            strcpy(v[x].cod,v[j].cod);
            v[x].cod[strlen(v[x].cod)]=dr;
            v[x].cod[strlen(v[x].cod)+1]='\0';
        }
   // for(j=0;j<m;j++)
      //  printf("v[%d]cod=%s\n",j,v[j].cod);
    for(i=0;i<n;i++)
    {
        z=(int)v[i].inf;
        strcpy(COD[z].inf,v[i].cod);
    }
    //for(i=0;i<128;i++)
       // printf("%c %s\n",(char)i,COD[i].inf);
    FILE *g;
    g=fopen("CodHuffman.txt", "w");
    for(i=0;i<strlen(s);i++)
    {
        z=(int)s[i];
        //printf("%c",s[i]);
        fprintf(g,COD[z].inf);
    }
    int radacina=m;
    fclose(f);
    fclose(g);
    FILE *h;
    h=fopen("radacina.txt", "w");
    fprintf(h,"%d",radacina);
    fclose(h);
    for(i=0;i<n;i++)
        {v[i].stanga=-1;
        v[i].dreapta=-1;}
    /// Aici se termina codificarea
}
char decod[100000];
void decodificare()
{
    codificare();
    int i,n;
    FILE *h,*f;
    f=fopen("radacina.txt","r");
    fscanf(f,"%d", &n);
    fclose(f);
    h=fopen("CodHuffman.txt","r");
    i=0;
     while(1)
   {
       if(feof(h))
      {
         break ;
      }
      decod[i++]=fgetc(h);
      printf("%c", decod[i-1]);
   }
    fclose(h);
    FILE *q;
    q=fopen("Afisarecod.txt","w");
    aux=v[n-1];
    for(i=0;i<strlen(decod);i++)
    {
        if(decod[i]=='0')
            aux=v[aux.stanga];
        if(decod[i]=='1')
            aux=v[aux.dreapta];
        if(aux.stanga==-1 || aux.dreapta==-1)
            fprintf(q,"%c",aux.inf),aux=v[n-1];
    }
    ///Aici se termina decodificarea
}
int main()
{
    int x=234;
    printf("Apasati 1 pentru codificare, sau 0 pentru decodificare:");
    scanf("%d", &x);
    printf("\n");
    if(x==1)
        {
            printf("\n"),codificare();
            printf(" \n\n\nSirul codificat poate fi vazut in fisierul ''CodHuffman.txt''");
        }
    if(x==0)
        {
            printf("\n"),decodificare();
            printf(" \n\n\nSirul decodificat poate fi vazut in fisierul ''Afisarecod.txt''");
        }
    printf("\n");
    if(x!=1 && x!=0)
    printf("Tastare gresita");
    return 0;
}
